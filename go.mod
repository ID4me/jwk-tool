module gitlab.com/ID4me/jwk-tool

go 1.12

require (
	github.com/gobs/pretty v0.0.0-20180724170744-09732c25a95b
	github.com/sbreitf1/errors v1.0.0 // indirect
	gitlab.com/denic-id/aim-client v0.1.1
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/tools v0.0.0-20190627033414-4874f863e654 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/square/go-jose.v2 v2.3.1
)
