package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/denic-id/aim-client/crypto"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
	jose "gopkg.in/square/go-jose.v2"
)

var (
	output  string
	keytype string
	keyfile string
	jwsfile string
	payload string
)

func main() {
	app := kingpin.New("jwk-tool", "jwk-tool")
	create := app.Command("create", "Create a jwk")
	create.Arg("type", "type of the key to create (rsa or ecdsa)").Default("rsa").StringVar(&keytype)
	create.Flag("output", "Output file to write the key to").Default("-").Short('o').StringVar(&output)

	load := app.Command("load", "load a key from file.")
	load.Arg("keyfile", "The filename to be loaded.").Required().StringVar(&keyfile)

	verify := app.Command("verify", "verify a JWS")
	verify.Flag("jwk", "The path to the JWK use to verify the JWS").Short('k').Required().StringVar(&keyfile)
	verify.Flag("jws", "The path to the JWS to verify.").Short('s').Required().StringVar(&jwsfile)
	verify.Flag("payload", "The payload to verify. If passed, the JWS is considered with detached payload").Short('p').StringVar(&payload)

	sign := app.Command("sign", "sign a payload")
	sign.Flag("jwk", "The path to the JWK to use").Short('k').Required().StringVar(&keyfile)
	sign.Flag("payload", "The payload to sign.").Short('p').Required().StringVar(&payload)

	operation := kingpin.MustParse(app.Parse(os.Args[1:]))

	switch operation {
	case "create":
		CreateKey()
	case "load":
		LoadKey()
	case "verify":
		Verify()
	case "sign":
		Sign()
	}

}

func errorExit(err error) {
	fmt.Println("ERROR: ", err.Error())
	panic(err)
	os.Exit(1)
}

func checkError(err error) {
	if err != nil {
		errorExit(err)
	}
}

func CreateKey() {
	jwk, err := crypto.CreateJwk(keytype)
	if err != nil {
		errorExit(err)
	}

	if output == "-" {
		jsonbuf, err := jwk.MarshalJSON()
		if err != nil {
			errorExit(crypto.MarshallKeyError)
		}

		fmt.Println(string(jsonbuf))
		os.Exit(0)
	}

	err = crypto.WriteKeyToFile(jwk, output)
	if err != nil {
		errorExit(err)
	}
}

func LoadKey() {
	jwk, err := crypto.ReadKeyFromFile(keyfile)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%#v\n", jwk)
}

func Verify() {
	jwk, err := crypto.ReadKeyFromFile(keyfile)
	if err != nil {
		errorExit(err)
	}

	jwsbytes, err := ioutil.ReadFile(jwsfile)
	if err != nil {
		errorExit(err)
	}

	obj, err := jose.ParseSigned(string(jwsbytes))
	if err != nil {
		errorExit(err)
	}

	//pretty.PrettyPrint(obj)

	if len(payload) > 0 {
		err = obj.DetachedVerify([]byte(payload), jwk.Public())
		if err != nil {
			errorExit(err)
		}
	} else {
		p, err := obj.Verify(jwk.Public())
		if err != nil {
			errorExit(err)
		}
		fmt.Println(string(p))
	}

	fmt.Printf("%#v\n", obj)

}

func Sign() {
	jwk, err := crypto.ReadKeyFromFile(keyfile)
	checkError(err)

	s, err := crypto.NewSigner(jwk)
	checkError(err)
	s.SetOption("method", "POST")
	jws, err := s.SignPayload(payload)
	checkError(err)

	out := jws.FullSerialize()
	checkError(err)

	fmt.Println("full serialized")
	fmt.Println(out)
	fmt.Println("----------------")

	out, err = jws.CompactSerialize()
	checkError(err)
	fmt.Println("compact serialized")
	fmt.Println(out)
	fmt.Println("----------------")

	header, p, err := s.DetachPayload(jws)
	checkError(err)

	fmt.Println("detached signature")
	fmt.Println(header)
	fmt.Println("")
	fmt.Println(p)

}
